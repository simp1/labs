#include "api.h"
#include "aes.h"

extern const uint8_t _localIntegrityKey[32];
extern const uint8_t _localConfidentialityKey[16];

myerror_t API::addKey(Key *key)
{
	_keys.push_back(key);
	return NO_ERROR;
}

myerror_t API::addUser(User *user)
{
	myerror_t ret = NO_ERROR;
	unsigned i;
	for ( i = 0 ; (i < _users.size()) && (ret == NO_ERROR) ; i++ ){
		if ( user->hasSameLogin(_users[i]->getLogin()) ){
			ret = ERROR_PARAM;
		}
	}
	if ( ret == NO_ERROR ){
		_users.push_back(std::move(user));
	}
	return ret;
}

myerror_t API::findKey(uint32_t id, Key **key)
{
	/** <ol> */
	myerror_t ret = ERROR_PARAM;
	
	*key = NULL;
	for ( auto it = _keys.begin() ; (it != _keys.end()) && (ret != NO_ERROR) ; it ++ ){
		if ( (*it)->getId() == id ){
			*key = *it;
		ret = NO_ERROR;
		}
	}
	
	/** </ol> */
	return ret;
}

myerror_t API::protectData(uint8_t *data, uint32_t length)
{
	myerror_t ret = ERROR_UNEXPECTED;
	uint8_t buffer[16];
	int remaining = length;
	uint32_t index = 0;
	
	while ( remaining > 0 ){
		for ( unsigned i = 0 ; i < 16 ; i ++ ){
			if ( index < length )
				buffer[i] = data[index];
			else
				buffer[i] = 0;
			index ++;
		}
		AES::encrypt(_localConfidentialityKey,buffer,buffer);
		for ( unsigned i = 0 ; i < 16 ; i ++ ){
			data[index-16+i] = buffer[i];
			remaining --;
			if ( remaining == 0 )
				break;
		}
	}
	
	ret = NO_ERROR;
	
	return ret;
}

myerror_t API::decryptKey(const Key *key, uint8_t *output, uint32_t *length)
{
	/** <ol> */
	myerror_t ret = ERROR_UNEXPECTED;
	uint8_t buffer[16];
	int remaining = key->_length;
	uint32_t index = 0;
	
	if ( *length < key->_length )
		ret = ERROR_PARAM;
	else{
		while ( remaining > 0 ){
			for ( unsigned i = 0 ; i < 16 ; i ++ ){

				if ( index < key->_length )
					buffer[i] = key->getValue()[index];
				else
					buffer[i] = 0;
				index ++;
			}
			AES::decrypt(_localConfidentialityKey,buffer,buffer);
			for ( unsigned i = 0 ; i < 16 ; i ++ ){
				output[index-16+i] = buffer[i];
				remaining --;
				if ( remaining == 0 )
					break;
			}
		}
	}
	
	ret = NO_ERROR;
	/** </ol> */
	return ret;
}

myerror_t check_pin(uint8_t pin[6], uint8_t ref_pin[6],uint8_t hack)
{
	/** <ol> */
	myerror_t ret = ERROR_UNEXPECTED;
	volatile unsigned h;
	bool isEqual = true;
	for ( unsigned i = 0 ; (i < 6) && isEqual ; i++ ){
		for ( h = 0 ; h < 1<<(hack) ; h ++ )
			;
		if ( pin[i] != ref_pin[i] )
			isEqual = false;
	}
	if (isEqual) ret = NO_ERROR;
	else ret = ERROR_SEC;
	/** </ol> */
	return ret;
}

