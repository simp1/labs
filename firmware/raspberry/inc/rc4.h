/** \file rc4.h
 * RC4 symmetric encryption
 *
 * Functions for encrypting using RC4 streamcipher.
 */

#ifndef RC4_H
#define RC4_H_

#include <cstdint>
#include "error.h"

/** \brief AES class */
class RC4
{
	public:

		/** \brief Function used to encrypt a plaintext stream into an encrypted
		  * stream using RC4.
		  *
		  * This function encrypts the provided plaintext using the given key 
		  * and RC4.
		  * <HR>
		  * \param key_size is the size of the key (in bytes)
		  * \param key is a key_size-byte long array containing the key.
		  * \param plain_size is the size of the plaintext (in bytes)
		  * \param plaintext is a plain_size-byte long array containing the 
		  * plaintext to encrypt.
		  * \param ciphertext is a plain_size-byte long array that will contain 
		  * the encrypted message.
		  *
		  * <HR>
		  */
		static myerror_t encrypt(
			uint32_t key_size, 
			const uint8_t *key, 
			uint32_t plain_size, 
			uint8_t *data);

		/** \brief Auto test function to test RC4 implementation w.r. to few 
		  * test vectors.
		  *
		  * This function uses test vectors defined in the class and test 
		  * implementation versus them.
		  * <HR>
		  * \return 
		  * <ul>
		  * <li> #ERROR_UNEXPECTED if a mismatch is found
		  * <li> #NO_ERROR if everything matches
		  * </ul>
		  *
		  * <HR>
		  */
		static myerror_t test();
	
	private:
		/* Test vectors */
		/** \brief Key used in test vectors */
		static const uint8_t TV_key[5];
		/** \brief Stream used in test vectors */
		static const uint8_t TV_stream[16];
};

#endif
