#include "api.h"
#include "xml.h"

#include <cstdio>

#include "user.h"
#include "hmac.h"

extern const uint8_t _localIntegrityKey[32];
extern const uint8_t _localConfidentialityKey[16];

myerror_t API::load(const std::string &userFile, const std::string &keyFile)
{
	myerror_t error = ERROR_UNEXPECTED;
	HMAC tagComputer;
	BASE64 converter;
	uint32_t length;
	uint8_t bufferUser[User::_serializedSize];
	uint8_t bufferKey[Key::_serializedSize+Key::_maxLength];
	uint8_t computedTag[tagComputer.tagLength];
	uint8_t providedTag[tagComputer.tagLength];

	/** <li> Clear the API </li> */
	_instanceId = 0;
	_users.clear();
	_keys.clear();
	
	try{
		/** <li> Init tag computation </li> */
		tagComputer.init(_localIntegrityKey,32);
		
		DomParser parser;
		parser.set_validate();
		parser.parse_file(userFile);
		if(parser){
			const Node* rootNode = parser.get_document()->get_root_node();
			const Element::AttributeList& attributes = dynamic_cast<const Element*>(rootNode)->get_attributes();
			const Attribute* attribute = *attributes.begin();
			_instanceId = atol(attribute->get_value().c_str());
			tagComputer.update((uint8_t*)&_instanceId,4);
			const NodeSet userNodes = rootNode->find("user");
			for ( const Node* node: userNodes ){
				User* newUser = new User();
				newUser->importFromObject<Node>(node);
				this->addUser(newUser);
				newUser->serialize(bufferUser,User::_serializedSize);
				tagComputer.update(bufferUser,User::_serializedSize);
			}
			/** <li> Compute the tag corresponding to the file </li> */
			tagComputer.final(computedTag);
			/** <li> Get the tag provided in the file </li> */
			const Node* tagNode = rootNode->find("tag")[0];
			const Node* valueNode = tagNode->find("value")[0];
			auto value = dynamic_cast<const Element*>(valueNode)->get_child_text()->get_content(); 
			converter.decode(value.c_str(), value.size(), providedTag, &length);
			
			uint8_t index = 0;
			uint8_t diff = 0;
			for ( index = 0 ; index < tagComputer.tagLength ; index ++ ){
				diff = computedTag[index] ^ providedTag[index];
			}
			if ( 0 != diff )
				error = ERROR_SEC;
		}
	}
	catch(const std::exception& ex){
		printf("Exception caught: %s\n",ex.what());
	}
	
	if ( ERROR_SEC ==  error )
		return error;
	
	try{
		/** <li> Init tag computation </li> */
		tagComputer.init(_localIntegrityKey,32);

		DomParser parser;
		parser.set_validate();
		parser.parse_file(keyFile);
		if(parser){
			const Node* rootNode = parser.get_document()->get_root_node();
			const Element::AttributeList& attributes = dynamic_cast<const Element*>(rootNode)->get_attributes();
			const Attribute* attribute = *attributes.begin();
			_instanceId = atol(attribute->get_value().c_str());
			tagComputer.update((uint8_t*)&_instanceId,4);
			const NodeSet keyNodes = rootNode->find("key");
			for ( const Node* node: keyNodes ){
				Key* newKey = new Key();
				newKey->importFromObject<Node>(node);
				this->addKey(newKey);
				newKey->serialize(bufferKey,Key::_serializedSize+Key::_maxLength);
				tagComputer.update(bufferKey,Key::_serializedSize+newKey->_length);
			}
			/** <li> Compute the tag corresponding to the file </li> */
			tagComputer.final(computedTag);
			/** <li> Get the tag provided in the file </li> */
			const Node* tagNode = rootNode->find("tag")[0];
			const Node* valueNode = tagNode->find("value")[0];
			auto value = dynamic_cast<const Element*>(valueNode)->get_child_text()->get_content(); 
			converter.decode(value.c_str(), value.size(), providedTag, &length);
			
			uint8_t index = 0;
			uint8_t diff = 0;
			for ( index = 0 ; index < tagComputer.tagLength ; index ++ ){
				diff |= computedTag[index] ^ providedTag[index];
			}
			if ( 0 != diff )
				error = ERROR_SEC;
			else
				error = NO_ERROR;
		}
	}
	catch(const std::exception& ex){
		printf("Exception caught: %s\n",ex.what());
	}

	return error;
}

myerror_t API::save(const std::string &userFile, const std::string &keyFile)
{
	myerror_t error = ERROR_UNEXPECTED;
	HMAC tagComputer;
	uint8_t bufferUser[User::_serializedSize];
	uint8_t bufferKey[Key::_serializedSize+Key::_maxLength];
	uint8_t computedTag[tagComputer.tagLength];
	BASE64 converter;
	char tagString[tagComputer.tagLength*2];
	uint32_t tagStringLength;
	char instanceIdString[11];
	sprintf(instanceIdString,"%d",_instanceId);


	{
	/* USERS */

	/** <li> Init tag computation </li> */
	tagComputer.init(_localIntegrityKey,32);

	Document document;
	document.set_internal_subset("userlist", "", "users.dtd");
	Element *nodeRoot, *element;

	nodeRoot = document.create_root_node("userlist");
	nodeRoot->set_attribute("api_id",instanceIdString);
	nodeRoot->add_child_text("\n");
	tagComputer.update((uint8_t*)&_instanceId,4);

	for ( auto it = _users.begin() ; it != _users.end() ; ++it ){
		
		nodeRoot->add_child_text("\t");
		element = nodeRoot->add_child("user");
		(*it)->exportToObject(element);
		nodeRoot->add_child_text("\n");
		(*it)->serialize(bufferUser,User::_serializedSize);
		tagComputer.update(bufferUser,User::_serializedSize);
	}

	/** tag **/
	tagComputer.final(computedTag);
	converter.encode(computedTag,tagComputer.tagLength,tagString,&tagStringLength);
	tagString[tagStringLength] = 0;
	nodeRoot->add_child_text("\t");
	element = nodeRoot->add_child("tag");
	Element *child;
	element->add_child_text("\n\t\t");
	child = element->add_child("value");
	child->add_child_text(tagString);
	element->add_child_text("\n\t");
	nodeRoot->add_child_text("\n");

	document.write_to_file(userFile);
	}

	{
	/* KEYS */

	/** <li> Init tag computation </li> */
	tagComputer.init(_localIntegrityKey,32);

	Document document;
	document.set_internal_subset("keylist", "", "keys.dtd");
	Element *nodeRoot, *element;

	nodeRoot = document.create_root_node("keylist");
	nodeRoot->set_attribute("api_id",instanceIdString);
	nodeRoot->add_child_text("\n");
	tagComputer.update((uint8_t*)&_instanceId,4);

	for ( auto it = _keys.begin() ; it != _keys.end() ; ++it ){
		nodeRoot->add_child_text("\t");
		element = nodeRoot->add_child("key");
		(*it)->exportToObject(element);
		nodeRoot->add_child_text("\n");
		(*it)->serialize(bufferKey,Key::_serializedSize+Key::_maxLength);
		tagComputer.update(bufferKey,Key::_serializedSize+(*it)->_length);
	}

	/** tag **/
	tagComputer.final(computedTag);
	converter.encode(computedTag,tagComputer.tagLength,tagString,&tagStringLength);
	tagString[tagStringLength] = 0;
	nodeRoot->add_child_text("\t");
	element = nodeRoot->add_child("tag");
	Element *child;
	element->add_child_text("\n\t\t");
	child = element->add_child("value");
	child->add_child_text(tagString);
	element->add_child_text("\n\t");
	nodeRoot->add_child_text("\n");

	document.write_to_file(keyFile);
	}

	error = NO_ERROR;

	return error;
}

