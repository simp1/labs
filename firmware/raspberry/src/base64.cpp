#include "base64.h"

#include <cstdlib>

myerror_t BASE64::test()
{
	myerror_t err = ERROR_UNEXPECTED;
	unsigned i, j;

	uint8_t input[60], result[60];
	char output[80];
	uint32_t size_64, size_str, size_res;
	const uint32_t nb_tests = 50;

	BASE64 temp;
	srand(32);
	for ( i = 0 ; i < nb_tests ; i++ ){
		size_str = rand() % 60;
		for ( j = 0 ; j < size_str ; j++ ){
			input[j] = rand();
		}
		temp.encode(input,size_str,output,&size_64);
		output[size_64+1] = 0;
		temp.decode(output,size_64,result,&size_res);

		if ( size_str != size_res ){
			goto end;
		}
		for ( j = 0 ; j < size_str ; j++ )
			if ( input[j] != result[j] )
				goto end;
	}
	err = NO_ERROR;
end:
	return err;
}

static const unsigned char from_base64[] = { 
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,  62, 255,  62, 255,  63,
	 52,  53,  54,  55,  56,  57,  58,  59,  60,  61, 255, 255, 255, 255, 255, 255,
	255,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
	 15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25, 255, 255, 255, 255,  63,
	255,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,
	 41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51, 255, 255, 255, 255, 255};


static const char to_base64[] = 
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"0123456789+/";


void BASE64::encode(const uint8_t *data_in, uint32_t length_in, char* data_out, uint32_t *length_out)
{
	uint32_t nb_rem_bytes = length_in;
	uint32_t step = 0;
	while ( nb_rem_bytes >= 3 ){
		unsigned char b3[3];
		b3[0] = data_in[3*step+0];
		b3[1] = data_in[3*step+1];
		b3[2] = data_in[3*step+2];
		data_out[4*step+0] = to_base64[ ((b3[0] & 0xfc) >> 2) ];
		data_out[4*step+1] = to_base64[ ((b3[0] & 0x03) << 4) + ((b3[1] & 0xf0) >> 4) ];
		data_out[4*step+2] = to_base64[ ((b3[1] & 0x0f) << 2) + ((b3[2] & 0xc0) >> 6) ];
		data_out[4*step+3] = to_base64[ ((b3[2] & 0x3f)) ];
		step ++;
		nb_rem_bytes -= 3;
	}
	*length_out = 4*step;
	switch ( nb_rem_bytes){
		case 1:
			data_out[*length_out] = to_base64[ ((data_in[3*step+0] & 0xfc) >> 2) ];
			*length_out += 1;
			data_out[*length_out] = to_base64[ ((data_in[3*step+0] & 0x03) << 4) ];
			*length_out += 1;
			data_out[*length_out] = '=';
			*length_out += 1;
			data_out[*length_out] = '=';
			*length_out += 1;
			break;
		case 2:
			data_out[*length_out] = to_base64[ ((data_in[3*step+0] & 0xfc) >> 2) ];
			*length_out += 1;
			data_out[*length_out] = to_base64[ ((data_in[3*step+0] & 0x03) << 4) + ((data_in[3*step+1] & 0xf0) >> 4) ];
			*length_out += 1;
			data_out[*length_out] = to_base64[ ((data_in[3*step+1] & 0x0f) << 2) ];
			*length_out += 1;
			data_out[*length_out] = '=';
			*length_out += 1;
			break;
		case 0:
		default:
			;
	}
}

void BASE64::decode(const char *data_in, uint32_t length_in, uint8_t* data_out, uint32_t *length_out)
{
	uint32_t index = 0;
	if ( length_in % 4 != 0 ){
		// TODO ERROR
	}
	for ( uint32_t i = 0 ; i < length_in ; i += 4 ){
		unsigned char b4[4];
		b4[0] = from_base64[data_in[i+0]];
		b4[1] = from_base64[data_in[i+1]];
		b4[2] = from_base64[data_in[i+2]];
		b4[3] = from_base64[data_in[i+3]];
		unsigned char b3[3];
		b3[0] = ((b4[0] & 0x3f) << 2) + ((b4[1] & 0x30) >> 4);
		b3[1] = ((b4[1] & 0x0f) << 4) + ((b4[2] & 0x3c) >> 2);
		b3[2] = ((b4[2] & 0x03) << 6) + ((b4[3] & 0x3f));
		data_out[index] = b3[0];
		index ++;
		if ( '=' != data_in[i+2] ){
			data_out[index] = b3[1];
			index ++;
		}
		if ( '=' != data_in[i+3] ){
		 	data_out[index] = b3[2];
			index ++;
		}
	}
	*length_out = index;
}

