#include "io.h"
#include "api.h"

const char *default_port = "/dev/ttyGS0";

#include <cstdio>
#include <fstream>

#define BUFFER1_LEN 2048
#define BUFFER2_LEN 8192

#define API_PIN 0X060A
extern myerror_t check_pin(uint8_t pin[6], uint8_t ref_pin[6], uint8_t hack);
#include "rc4.h"
#define API_RC4 0X0701


myerror_t tests_admin();
myerror_t tests_user();

#include <time.h>
#define clock(x) clock_gettime(CLOCK_REALTIME, &gettime_now);\
                 x = gettime_now.tv_nsec;

uint32_t data_length;
int main()
{
	myerror_t error = NO_ERROR;
	API api;

	uint32_t command;
	uint8_t nb_out;
	uint8_t charac;
	bool go_on = true;
	uint8_t buffer1[BUFFER1_LEN];
	uint8_t buffer2[BUFFER2_LEN];
	uint32_t id1, id2;
	key_type_t key_type;
	key_use_t key_use;
	date_t key_validity;
	uint32_t length,key_length;
	FILE *in;
	FILE *out;
	uint32_t word;
	uint8_t pin_code[6], ref_code[6] = {1,2,7,0,9,8};
	struct timespec gettime_now;
	uint32_t tic, tac, diff;

	/* GPIO object */
	GPIO gpio;

	/* Create the serial connexion and wait for the synchronization pattern */
	Serial serial(default_port);

	/* Entering the main loop waiting for commands */
	while ( go_on ){
		gpio.set_led(GPIO::GREEN_LED);
		length = sizeof(word);
		serial.get_data((uint8_t*)&word,&length);
		command=word;
		if ( CMD_HEADER != (command & CMD_HEADER_MASK) ){
			printf("[error] Waiting for command, received %08x\n",command);
			// error !!! 
		}
		else{
			gpio.set_led(GPIO::LED_OFF);
			switch( command & API_CMD_MASK ){
				
				case API_PING:
					printf("[cmd] API_PING\n");
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data((uint8_t*)"pong",5u));
					break;
				
				case API_RESET:
					printf("[cmd] API_RESET\n");
					error = myerror_t(error | api.reset());
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					break;
				
				case API_LOAD:
					printf("[cmd] API_LOAD\n");
					if ( NO_ERROR == error ){
						word = sizeof(buffer1);
						error = myerror_t(error | serial.get_data(buffer1,&word));
						data_length = word;
					}
					if ( NO_ERROR == error ){
						out = fopen(api._userFile.c_str(),"w");
						fprintf(out,"%s",(char*)buffer1);
						fclose(out);
					}
					if ( NO_ERROR == error ){
						word = sizeof(buffer2);
						error = myerror_t(error | serial.get_data(buffer2,&word));
						data_length = word;
					}
					if ( NO_ERROR == error ){
						out = fopen(api._keyFile.c_str(),"w");
						fprintf(out,"%s",(char*)buffer2);
						fclose(out);
						error = myerror_t(error | api.load(api._userFile, api._keyFile));
					}
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					break;
				
				case API_SAVE:
					printf("[cmd] API_SAVE\n");
					if ( NO_ERROR == error ){
						in = fopen(api._userFile.c_str(),"r");
						fseek(in, 0L, SEEK_END);
						data_length = ftell(in);
						if ( data_length > BUFFER1_LEN )
						  error = myerror_t(error | ERROR_MEM);
					}
					if ( NO_ERROR == error ){
						fseek(in, 0L, SEEK_SET);
						fread(buffer1,1,data_length,in);
					}
					else data_length = 1; // error case
					nb_out = 2;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data(buffer1,data_length));
					if ( NO_ERROR == error ){
						fclose(in);
					}
					if ( NO_ERROR == error ){
						in = fopen(api._keyFile.c_str(),"r");
						fseek(in, 0L, SEEK_END);
						data_length = ftell(in);
						if ( data_length > BUFFER2_LEN )
						  error = myerror_t(error | ERROR_MEM);
					}
					if ( NO_ERROR == error ){
						fseek(in, 0L, SEEK_SET);
						fread(buffer2,1,data_length,in);
					}
					else data_length = 1;
					error = myerror_t(error | serial.send_data(buffer2,data_length));
					if ( NO_ERROR == error ){
						fclose(in);
					}
					break;
				
				case API_ADD_USR:
					printf("[cmd] API_ADD_USR\n");
					length = sizeof(buffer1)-1;
					error = myerror_t(error | serial.get_data(buffer1,&length));
					if ( NO_ERROR == error ){
						buffer1[length] = 0;
						length = sizeof(buffer2)-1;
						error = myerror_t(error | serial.get_data(buffer2,&length));
					}
					if ( NO_ERROR == error ){
						buffer2[length] = 0;
						error = myerror_t(error | api.newUser(std::string((char*)buffer1),std::string((char*)buffer2),&id1));
					}
					if ( NO_ERROR == error ){
						printf("[inf] Creation of user %s (%d)\n",(char*)buffer1,id1);
					}
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data((uint8_t*)&id1,4u));
					break;
				
				case API_DEL_USR:
					printf("[cmd] API_DEL_USR\n");
					length = sizeof(id1);
					error = myerror_t(error | serial.get_data((uint8_t*)&id1,&length));
					if ( NO_ERROR == error ){
					  error = myerror_t(error | api.deleteUser(id1));
					}
					if ( NO_ERROR == error ){
						printf("[inf] Deletion of user id %d\n",id1);
					}
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					break;
				
				case API_LOGIN:
					printf("[cmd] API_LOGIN\n");
					length = sizeof(buffer1)-1;
					error = myerror_t(error | serial.get_data(buffer1,&length));
					if ( NO_ERROR == error ){
						buffer1[length] = 0;
						length = sizeof(buffer2)-1;
						error = myerror_t(error | serial.get_data(buffer2,&length));
					}
					if ( NO_ERROR == error ){
						buffer2[length] = 0;
						error = myerror_t(error | api.connectUser(std::string((char*)buffer1),std::string((char*)buffer2)));
					}
					if ( NO_ERROR == error ){
						printf("[inf] Logged as %s\n",(char*)buffer1);
					}
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					break;
				
				case API_LOGOUT:
					printf("[cmd] API_LOGOUT\n");
					error = myerror_t(error | api.disconnect());
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					break;
				
				case API_GEN_KEY:
					printf("[cmd] API_GEN_KEY\n");
					length = sizeof(word);
					error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
					key_type = key_type_t(word);
					if ( NO_ERROR == error ){
						length = sizeof(word);
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						key_use = key_use_t(word);
					}
					if ( NO_ERROR == error ){
						length = sizeof(word); // strange overflow if directly writing to key_validity
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						key_validity = word;
					}
					if ( NO_ERROR == error ){
						length = sizeof(key_length);
						error = myerror_t(error | serial.get_data((uint8_t*)&key_length,&length));
					}
					if ( NO_ERROR == error ){
					  error = myerror_t(error | api.newKey(key_type, key_use, key_validity, key_length, &id1));
					}
					if ( NO_ERROR == error ){
						printf("[inf] Generation of key %d\n",id1);
					}
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data((uint8_t*)&id1,4u));
					break;
				
				case API_IMP_KEY:
					printf("[cmd] API_IMP_KEY\n");
					length = sizeof(word);
					error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
					id1 = word;
					if ( NO_ERROR == error ){
						data_length = sizeof(buffer1);
						error = myerror_t(error | serial.get_data(buffer1,&data_length));
					}
					if ( NO_ERROR == error ){
						length = sizeof(word);
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						key_type = key_type_t(word);
					}
					if ( NO_ERROR == error ){
						length = sizeof(word);
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						key_use = key_use_t(word);
					}
					if ( NO_ERROR == error ){
						length = sizeof(word);
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						key_validity = word;
					}
					if ( NO_ERROR == error ){
					  error = myerror_t(error | api.unwrap(id1, buffer1, data_length, key_type, key_use, key_validity, &id2));
					}
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data((uint8_t*)&id2,4u));
					break;
				
				case API_EXP_KEY:
					printf("[cmd] API_EXP_KEY\n");
					length = sizeof(word);
					error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
					id1 = word;
					if ( NO_ERROR == error ){
						length = sizeof(word);
						error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
						id2 = word;
					}
					if ( NO_ERROR == error ){
						data_length = sizeof(buffer1);
						error = myerror_t(error | api.wrap(id1, id2, buffer1, &data_length));
					}
					else data_length = 1; // error case
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data(buffer1,data_length));
					break;
				
				case API_DAT_ENC:
					printf("[cmd] API_DAT_ENC\n");
					length = sizeof(word);
					error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
					id1 = word;
					if ( NO_ERROR == error ){
						data_length = sizeof(buffer1);
						error = myerror_t(error | serial.get_data(buffer1,&data_length));
					}
					if ( NO_ERROR == error ){
					  error = myerror_t(error | api.encrypt(id1, buffer1, data_length));
					}
					else data_length = 1; // error case
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data(buffer1,data_length));
					break;
				
				case API_DAT_DEC:
					printf("[cmd] API_DAT_DEC\n");
					length = sizeof(word);
					error = myerror_t(error | serial.get_data((uint8_t*)&word,&length));
					id1 = word;
					if ( NO_ERROR == error ){
						data_length = sizeof(buffer1);
						error = myerror_t(error | serial.get_data(buffer1,&data_length));
					}
					if ( NO_ERROR == error ){
					  error = myerror_t(error | api.decrypt(id1, buffer1, data_length));
					}
					else data_length = 1; // error case
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data(buffer1,data_length));
					break;
				
				case API_DAT_HASH:
					printf("[cmd] API_DAT_HASH\n");
					nb_out = 0;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					// TODO
					break;
				case API_RC4:
					printf("[cmd] API_RC4\n");
					key_length = sizeof(buffer1);
					error = myerror_t(error | serial.get_data(buffer1,&key_length));
					if ( NO_ERROR == error ){
						data_length = sizeof(buffer2);
						error = myerror_t(error | serial.get_data(buffer2,&data_length));
					}
					if ( NO_ERROR == error ){
					  error = myerror_t(error | RC4::encrypt(key_length, buffer1, data_length, buffer2));
					}
					else data_length = 1; // error case
					nb_out = 1;
					error = myerror_t(error | serial.send_data(&nb_out,1));
					error = myerror_t(error | serial.send_data(buffer2,data_length));
					break;
				default:
					if ( (API_PIN & 0xFF00) == (command & 0xFF00) ){
						printf("[cmd] API_PIN\n");
						data_length = sizeof(pin_code);
						error = myerror_t(error | serial.get_data(pin_code,&data_length));
						printf("[debug] read pin code of size %d\n",data_length);
						if ( NO_ERROR == error ){
							printf("[debug] correct first pin byte %d\n",ref_code[0]);
							clock(tic);
							error = myerror_t(error | 
								check_pin(pin_code,ref_code,command & 0xFF));
							clock(tac);
							diff = tac - tic;
						}
						nb_out = 1;
						error = myerror_t(error | serial.send_data(&nb_out,1));
						error = myerror_t(error | serial.send_data((uint8_t*)&diff,4u));
					}
					break;
			}
			/* Handle error */
			printf("sending error_code: %d\n",error);
			serial.send_data((uint8_t *)&error,4);
			if ( NO_ERROR == error ){
				api.save(api._userFile,api._keyFile);
			}
			error = NO_ERROR;
		}
	}
	
	return 0;
}



