/** \file user.h
 * User definition
 *
 * User class definition.
 */

#ifndef USER_H
#define USER_H

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include "error.h"
#include "sha256.h"

/** \brief Maximum length of a user login. */
constexpr unsigned MAX_LOGIN_LEN = 31;
/** \brief Maximum length of a user password. */
constexpr unsigned MAX_PASSW_LEN = 50;

/** \brief User roles
 *
 * This enumerated type has positive values smaller than 256.
 * Hence, it can be converted to uint8_t type for dumping.
 */
enum role_t{
	/** \brief simple user */
	USER_BASIC = 0,
	/** \brief admin */
	USER_ADMIN = 1
};

/** \brief User class */
class User
{
	/* For I/O */
	friend std::ostream& operator<<(std::ostream& os, const User& obj);
	
public:
	/** \brief Default constructor: used to create an empty User before updating it from a JSON string for instance. 
	 *
	 * <HR>
	 */
	User() : _id(0) , _role(USER_BASIC) {};
	/** \brief This constructor creates a user from a login, id and role 
	 *
	 * <HR>
	 */
	User(const char login[MAX_LOGIN_LEN], const char password[MAX_PASSW_LEN], uint32_t id, role_t role);
	/** \brief User destructor 
	 *
	 * <HR>
	 */
	~User();
	/** \brief This function sets the given attribute to the provided value in class User.
	 *
	 * This function is for example used to construct User classes from a json_object_t.
	 * <HR>
	 * \param field is a string containing the name of the attribute
	 * \param value is the value that should be set to the mentioned attribute.
	 * \return 
	 * <ul>
	 * <li> #NO_ERROR if no error occured 
	 * </ul>
	 *
	 * <HR>
	 */
	myerror_t set(const std::string &field, const std::string &value);

	/** \brief Function returning the id of the user 
	 *
	 * <HR>
	 */
	uint32_t getId() const;

	/** \brief Function returning the login of the user 
	 *
	 * <HR>
	 */
	const char* getLogin() const;

	/** \brief Function returning the role of the user 
	 *
	 * <HR>
	 */
	const role_t getRole() const;
	
	/** \brief Function comparing two users' logins 
	 *
	 * <HR>
	 */
	bool hasSameLogin(const char *login) const;

	/** brief Function comparing provided password to the user one */
	bool isCorrectPassword(const std::string &password) const;

	/** \brief Serialize the User object to a byte array of \a _serializedUserSize bytes */
	myerror_t serialize(uint8_t *tab, uint32_t len);

	/** \brief This function parses an object to update an empty User class previously created.
	 *
	 * This function is for example used to construct User classes when loading the API from a file.
	 * <HR>
	 * \param in is the pointer to the input object containing the user data.
	 * \return an error code.
	 *
	 * <HR>
	 */
	template<class T>
	myerror_t importFromObject(const T* in);

	/** \brief This function export a User in an object.
	 *
	 * This function is for example used to export User classes when saving the API to a file.
	 * <HR>
	 * \param out is the pointer to the output object that will contain the user data. It must have been allocated before the call.
	 * \return an error code.
	 *
	 * <HR>
	 */
	template<class T>
	myerror_t exportToObject(T* out) const;

	/** \brief Size of the hashed password */
	static const size_t _hashPasswSize = SHA256::hashSize;
	/** \brief Size of the serialization of a User object */
	static const size_t _serializedSize = 4 + MAX_LOGIN_LEN + _hashPasswSize + 1;
private:
	/** \brief Id of the user, must be unique. */
	uint32_t _id;
	/** \brief Login of the user. 
	 *
	 * The size of a login is at most #MAX_LOGIN_LEN.
	 */
	char _login[MAX_LOGIN_LEN+1];
	/** \brief Role of the user. */
	role_t _role;
	/** \brief Password (hashed password) */
	uint8_t _password[_hashPasswSize];
	/** \brief Hashes the password */
	static void hashPassword(const char password[MAX_PASSW_LEN], uint8_t digest[_hashPasswSize]);
};


#endif
