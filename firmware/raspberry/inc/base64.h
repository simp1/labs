/** \file base64.h
 * Base 64 encoding an decoding utilities
 *
 * Functions for encoding and decoding from bytes to base 64.
 * Code based on part of the libb64 project which has been placed in the public domain.
 */

#ifndef BASE64_H
#define BASE64_H

#include <cstdint>
#include <cstddef>
#include "error.h"

/** \brief BASE64 class */
class BASE64
{
public:
  /** \brief Function used to encode a byte array.
   *
   * This function process the provided data to output its base64-encoded value.
   * <HR>
   * \param data_in is an array of bytes to process.
   * \param length_in is the number of bytes in the array data_in.
   * \param data_out is a string that will contain the encoded result.
   * \param length_out will be updated to contain the actual size of the encoded result.
   *
   * <HR>
   */ 
  void encode(const uint8_t *data_in, uint32_t length_in, char* data_out, uint32_t *length_out);
  /** \brief Function used to decode a base64-encoded string.
   *
   * This function process the provided string to output the corresponding value in a byte array.
   * <HR>
   * \param data_in is the string to process.
   * \param length_in is the number of characters to consider in the string.
   * \param data_out is a byte array that will contain the decoded value.
   * \param length_out will be updated to contain the actual byte-size of the decoded value.
   *
   * <HR>
   */ 
  void decode(const char *data_in, uint32_t length_in, uint8_t* data_out, uint32_t *length_out);
    /** \brief Auto test function to test Base 64 conversion w.r.t. randomly generated data. 
   *
   * This function encodes and decodes some random data then checks the original value is recovered.
   * <HR>
   * \return 
   * <ul>
   * <li> #ERROR_UNEXPECTED if a mismatch is found
   * <li> #NO_ERROR if everything matches
   * </ul>
   *
   * <HR>
   */
  static myerror_t test();
private:
};

#endif
