/** \file xml.h
 * XML utilities
 *
 * Functions linked to XML representation of the memory.
 */

#ifndef XML_H
#define XML_H

#include "base64.h"
#include "user.h"
#include "key.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <libxml++/libxml++.h>

using namespace xmlpp;

template<class Node>
myerror_t User::importFromObject(const Node* userNode)
{
	Node* node;
	node = userNode->find("id")[0];
	this->set("id",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = userNode->find("login")[0];
	this->set("login",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = userNode->find("password")[0];
	this->set("password",dynamic_cast<const Element*>(node)->get_child_text()->get_content());
	node = userNode->find("privilege")[0];
	this->set("privilege",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	return NO_ERROR;
}


template<class Element>
myerror_t User::exportToObject(Element* userNode) const
{
	char password[64];
	BASE64 converter;
	uint32_t length;
	converter.encode(_password,_hashPasswSize,password,&length);
	password[length] = 0;

	Element *child;
	userNode->add_child_text("\n\t");
	/* id */
	userNode->add_child_text("\t");
	child = userNode->add_child("id");
	child->add_child_text(std::to_string(_id));
	userNode->add_child_text("\n\t");
	/* login */
	userNode->add_child_text("\t");
	child = userNode->add_child("login");
	child->add_child_text(_login);
	userNode->add_child_text("\n\t");
	/* password */
	userNode->add_child_text("\t");
	child = userNode->add_child("password");
	child->add_child_text(password);
	userNode->add_child_text("\n\t");
	/* privilege */
	userNode->add_child_text("\t");
	child = userNode->add_child("privilege");
	child->add_child_text(std::to_string(_role));
	userNode->add_child_text("\n\t");
	
	return NO_ERROR;
}


template<class Node>
myerror_t Key::importFromObject(const Node* keyNode)
{
	Node* node;
	node = keyNode->find("id")[0];
	this->set("id",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("owner")[0];
	this->set("owner",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("type")[0];
	this->set("type",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("use")[0];
	this->set("use",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("validity")[0];
	this->set("validity",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("length")[0];
	this->set("length",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	node = keyNode->find("value")[0];
	this->set("value",dynamic_cast<const Element*>(node)->get_child_text()->get_content().c_str());
	
	return NO_ERROR;
}

template<class Element>
myerror_t Key::exportToObject(Element* keyNode) const
{
	char value[Key::_maxLength*2];
	BASE64 converter;
	uint32_t length;
	converter.encode(_value.data(),_length,value,&length);
	value[length] = 0;

	Element *child;
	keyNode->add_child_text("\n\t");

	/* id */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("id");
	child->add_child_text(std::to_string(_id));
	keyNode->add_child_text("\n\t");
	/* owner */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("owner");
	child->add_child_text(std::to_string(_owner));
	keyNode->add_child_text("\n\t");
	/* type */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("type");
	child->add_child_text(std::to_string(_type));
	keyNode->add_child_text("\n\t");
	/* use */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("use");
	child->add_child_text(std::to_string(_use));
	keyNode->add_child_text("\n\t");
	/* validity */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("validity");
	child->add_child_text(std::to_string(_validity));
	keyNode->add_child_text("\n\t");
	/* length */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("length");
	child->add_child_text(std::to_string(_length));
	keyNode->add_child_text("\n\t");
	/* value */
	keyNode->add_child_text("\t");
	child = keyNode->add_child("value");
	child->add_child_text(value);
	keyNode->add_child_text("\n\t");

	return NO_ERROR;
}

#endif
