# SIMP 1 Labs

Contains the material for lab sessions of SIMP 1 module.

## Install Instructions

### Prerequisite
You must have installed on your laptop:
  - a recent version of VirtualBox,
  - the VirtualBox Extension Pack,
  - the NewAE Chipwhisperer virtual machine (version 5.6.1).

### VM First Launch
First the VM must be configured.

  1. Launch the NewAE virtual machine.
  2. Connect to the machine with login/password `vagrant/vagrant`.
  3. Choose a password for jupyter-lab (you will need to enter it in your browser to access jupyter).
  4. Restart your VM.
  5. In a browser reach `localhost:8888` to check jupyter is running (and enter your freshly set password).

> **WARNING** Take care that the VM is configured for QWERTY keyboards ! Then via the browser the configuration will indeed be the one of your laptop.

### Installing material
Once connected to jupyter (step 4 above) you will have access to the root of the working directory.
From here launch a new terminal (button *New* at the top right corner).

    $ git clone https://gitlab.inria.fr/simp1/labs.git simp1

Then, you can quit the tab and go back to the file list.
There, a new directory `simp1` should have been created by the `git clone` command.

Browse this directory (by clicking on it) and open the `install.ipynb` notebook (also by clicking on it).

Then you must run each cell once to finalize installation.
Below are some basics on jupyter notebooks.

## Jupter Notebooks
A jupyter notebook is composed of cells which can either be of code or Markdown type.

To **edit** a cell, click on it and use your keyboard to insert characters.
A stroke on *Entry* will add a new line in the cell.

To **execute** a cell you have to press *Shift+Entry* or you can click in the top menu.



