# *-* coding: utf-8 *-*

import emul_serial as serial
import time
from struct import pack, unpack
from enum import IntEnum, unique

CMD_HEADER_MASK = 0xFFFF0000
CMD_HEADER = 0x80000000
API_CMD_MASK = 0x0000FFFF
MAX_DATA_LEN = 1024

@unique
class Command(IntEnum):
    API_PING     = 0x0011
    API_RESET    = 0x00FF
    API_LOAD     = 0x0101
    API_SAVE     = 0x0102
    API_ADD_USR  = 0x0201
    API_DEL_USR  = 0x0202
    API_LOGIN    = 0x0301
    API_LOGOUT   = 0x0302
    API_KEY_GEN  = 0x0401
    API_KEY_IMP  = 0x0402
    API_KEY_EXP  = 0x0403
    API_DAT_ENC  = 0x0501
    API_DAT_DEC  = 0x0502
#        'API_DAT_HASH' : 0X0503, # not yet implemented
    API_PIN      = 0x0610 # from 0x0618 to 0x0601 to increase difficulty
    API_RC4      = 0x0701

class KeyType(IntEnum):
    KEY_SYM = 0x1
    KEY_PUB = 0x2
    KEY_PRI = 0x3


class KeyUse(IntEnum):
    KEY_ENCR = 0x1
    KEY_SIGN = 0x2
    KEY_INTE = 0x3
    KEY_WRAP = 0x4

class SecureAPI(object):
    class APIError(Exception):
        class ErrorCode(IntEnum):
            """Evertything is ok."""
            NO_ERROR         = 0x00
            """An I/O error occured."""
            ERROR_IO         = 0x01
            """Provided parameters do not match pre-conditions."""
            ERROR_PARAM      = 0x02
            """The data provided did not match the expected format."""
            ERROR_FORMAT     = 0x04
            """A security error occured."""
            ERROR_SEC        = 0x08
            """A sequencing error occured."""
            ERROR_SEQ        = 0x10
            """A memory error occured."""
            ERROR_MEM        = 0x20
            """An unexpected error occured."""
            ERROR_UNEXPECTED = -1
        def __init__(self, err_code, *args, **kwargs):
            Exception.__init__(self,*args,**kwargs)
            if isinstance(err_code,self.ErrorCode):
                self.code = err_code
            else:
                self.code = self.ErrCode(err_code)
        def __str__(self):
            return "[api error] returned {}".format(self.code.name)
   
    def connect(self, port: str = '/dev/ttyACM0'):
        """Connects to the secure API device.
        
           Kwargs:
              - port: port path to connect to
        """
        self.__serial_connection = serial.Serial(port,
            baudrate=115200,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            timeout=1)
        while True:
            try:
                res = self.__send_command(Command.API_PING,())
                break
            except Exception as e:
                print(e)
                time.sleep(0.5)
        return None
    
    def reset(self):
        """Resets the API content to its initial state."""
        self.__send_command(Command.API_RESET)
        return None
    
    def save(self, user_filename: str, key_filename: str):
        """Gets API memory content as an user file and one key file (XML format).
        
           Args:
              - user_filename: filename for storing the user related memory
              - key_filename: filename for storing the key memory
        """
        [user_file_content,key_file_content] = self.__send_command(Command.API_SAVE,())
        with open(user_filename, "w") as out_file:
            out_file.write(user_file_content.decode('utf-8'))
        with open(key_filename, "w") as out_file:
            out_file.write(key_file_content.decode('utf-8'))
        return None
    
    def load(self, user_filename: str, key_filename: str):
        """Sets API memory content from a user file and a key file (XML format).
        
           Args:
              - user_filename: filename of the file containing the user memory to import
              - key_filename: filename of the file containing the key memory to import
        """
        with open(user_filename, "r") as in_file:
            user_file_content = in_file.read()
        with open(key_filename, "r") as in_file:
            key_file_content = in_file.read()
        self.__send_command(Command.API_LOAD,(user_file_content,key_file_content))
        return None
    
    def login(self, login: str, password: str):
        """Logs in the API.
        
           Args:
              - login: the login of the user
              - password: the corresponding password
        """
        self.__send_command(Command.API_LOGIN,(login,password))
        return None
    
    def logout(self):
        """Logs out from the API."""
        self.__send_command(Command.API_LOGOUT)
        return None
    
    def add_user(self, login: str, password: str):
        """Adds a user to the API.
        
           Args:
              - login: login of the user to create
              - password: password of the user to create
           
           Returns:
              The ID (integer) that must be use to identify the user in further API calls.
        """
        [user_id] = self.__send_command(Command.API_ADD_USR,(login,password))
        user_id = unpack('<I',user_id)[0]
        return user_id
    
    def delete_user(self, user_id: int):
        """Deletes a user from the API.
        
           Args:
              - user_id: the ID of the user to delete
        """
        self.__send_command(Command.API_DEL_USR,(user_id,))
        return None
    
    def new_key(self, key_type: KeyType, key_use: KeyUse, validity: int, length: int):
        """Creates a new key in the API and add it to the currently connected user.
        
           Args:
              - key_type: KeyType value determining the type of key (public/private/symmetric)
              - key_use: KeyUse value determining the usage of the key (kinf od cryptographic processing)
              - validity: integer containing the last year of the key validity
              - length: number of bytes to generate
           
           Returns:
              The ID (integer) that must be use to identify the key in further API calls.
        """
        try:
            key_type = KeyType(key_type)
            key_use = KeyUse(key_use)
        except Exception as e:
            raise e from e
        [key_id] = self.__send_command(Command.API_KEY_GEN,(key_type.value,key_use.value,validity,length))
        key_id = unpack('<I',key_id)[0]
        return key_id
    
    def encrypt(self, key_id: int, data: list):
        """Encrypts data with the specified key (symmetric).
        
           Args:
              - key_id: the ID of the key to use for encryption
              - data: list of bytes of data to encrypt
           
           Returns:
              The list of encrypted bytes.
        """
        if len(data) > MAX_DATA_LEN: raise Exception("[error] Data should be at most",MAX_DATA_LEN,"bytes long")
        data = bytes(data)
        [data] = self.__send_command(Command.API_DAT_ENC,(key_id,data))
        data = [ data[i] for i in range(len(data)) ]
        return data
    
    def decrypt(self, key_id: int, data: list):
        """Decrypts data with the specified key (symmetric).
        
           Args:
              - key_id: the ID of the key to use for decryption
              - data: list of bytes of data to decrypt
           
           Returns:
              The list of plaintext bytes.
        """
        if len(data) > MAX_DATA_LEN: raise Exception("[error] Data should be at most",MAX_DATA_LEN,"bytes long")
        data = bytes(data)
        [data] = self.__send_command(Command.API_DAT_DEC,(key_id,data))
        data = [ data[i] for i in range(len(data)) ]
        return data
    
    def key_export(self, wrap_id: int, key_id: int):
        """Exports a specified key using a key wrap.
        
           Args:
              - wrap_id: the ID of the key to use for wrapping
              - key_id: the ID of the key to export
           
           Returns:
              A list of bytes corresponding to the wrapped key.
        """
        [data] = self.__send_command(Command.API_KEY_EXP,(wrap_id,key_id))
        data = [ data[i] for i in range(len(data)) ]
        return data
    
    def key_import(self, wrap_id: int, data: list, key_type: KeyType, key_use: KeyUse, key_validity: int):
        """Imports a wrap content using a key wrap.
        
           Args:
              - wrap_id: the ID of the key to use for wrapping
              - data: list of bytes generated by a call to `key_export`
              - key_use: KeyUse value determining the usage of the key (kinf od cryptographic processing)
              - validity: integer containing the last year of the key validity
              - length: number of bytes to generate
           
           Returns:
              The ID (integer) that must be use to identify the freshly imported key in further API calls.
        """
        try:
            key_type = KeyType(key_type)
            key_use = KeyUse(key_use)
        except Exception as e:
            raise e from e
        data = bytes(data)
        [key_id] = self.__send_command(Command.API_KEY_IMP,(wrap_id,data,key_type.value,key_use.value,key_validity))
        key_id = unpack('<I',key_id)[0]
        return key_id
    
    def check_pin(self, pin: list, command = Command.API_PIN.value):
        """Checks a 6-digit PIN code against a reference code inside the device.
        
            Args:
               - pin: 6-digit list guess
            
            Returns:
               A pair of values [ok,time] with *ok* being a boolean to True if the pin is correct
               and *time* containing the measured comparison time.
        """
        if len(pin) != 6: raise Exception("[error] PIN are composed of 6 digits")
        if command < 0x0601 or command > 0x0618: raise Exception("[error] Command is not a PIN command")
        class CommandHack():
            def __init__(self,value): self.value = value
        command_hack = CommandHack(command)
        data = ''
        for digit in pin:
            data += chr(digit)
        ok = True
        try:
            [time] = self.__send_command(command_hack,(data,))
        except self.APIError:
            [time] = self.data_out
            ok = False
        time = unpack('<I',time)[0]
        return [ok,time]
    
    def rc4_encrypt(self, key: list, data: list):
        """! OUT OF API ! Encrypts data using RC4 encryption scheme.
        
            Args:
               - key: list of bytes corresponding to the key
               - data: list of bytes corresponding to the data to encrypt
            
            Returns:
              The list of encrypted bytes.
        """
        if len(key) > MAX_DATA_LEN: raise Exception("[error] Key should be at most",MAX_DATA_LEN,"bytes long")
        if len(data) > MAX_DATA_LEN: raise Exception("[error] Data should be at most",MAX_DATA_LEN,"bytes long")
        key = bytes(key)
        data = bytes(data)
        [data] = self.__send_command(Command.API_RC4,(key,data))
        data = [ data[i] for i in range(len(data)) ]
        return data
    
    ### TOOLS ###
    def flush(self):
        self.__serial_connection.flushInput()
        self.__serial_connection.flushOutput()
    
    ### PRIVATE FUNCTIONS (do not touch/read) ###
    def __init__(self,port='/dev/ttyACM0'):
        self.__serial_connection = None
    
    def __del_(self):
        if self.__serial_connection is not None:
            self.__serial_connection.close()
    
    def __write(self,data):
        size = pack('<I',len(data))
        self.__serial_connection.write(size)
        self.__serial_connection.write(data)
        ack = self.__serial_connection.read(4)
        ack = unpack('<I',ack)[0]
        if len(data) != ack:
            raise Exception("Wrong ack: received "+str(ack)+" instead of "+str(len(data)))
    
    def __read(self):
        size = self.__serial_connection.read(4)
        size =  unpack('<I',size)[0]
        data = self.__serial_connection.read(size)
        return data
    
    def __send_command(self,command: Command, data_in=None):
        try:
            opcode = CMD_HEADER + command.value
        except Exception as e:
            raise e from e
        opcode = pack('<I',opcode)
        self.__write(opcode)
        if data_in is not None:
            for datum in data_in:
                if int == type(datum):
                    datum = pack('<I',datum)
                elif str == type(datum):
                    datum = datum.encode('utf-8')
                self.__write(datum)
        self.data_out = []
        nb_out = self.__read()
        nb_out = unpack('B',nb_out)[0]
        for idx in range(nb_out):
            self.data_out.append(self.__read())
        error_code = self.__read()
        error_code = unpack('<I',error_code)[0]
        error_code = self.APIError.ErrorCode(error_code)
        if error_code:
            raise self.APIError(error_code)
        return self.data_out

