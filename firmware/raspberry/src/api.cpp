#include "api.h"
#include "aes.h"

myerror_t API::reset()
{
	myerror_t cr = ERROR_UNEXPECTED;

	cr = this->load(_defaultUserFile,_defaultKeyFile);
	if ( NO_ERROR == cr ){
		_connectedUser = NULL;
		cr = this->save(_userFile,_keyFile);
	}
	return cr;
}

myerror_t API::connectUser(const std::string &login, const std::string &password)
{
	myerror_t cr = NO_ERROR;
	unsigned i;
	
	/* Test that no users are currently connected */
	if ( NULL != _connectedUser ){
		cr = ERROR_SEQ;
	}
	
	/* Find user in the base */
	if ( NO_ERROR == cr ){
		cr = ERROR_PARAM;
		for ( i = 0 ; i < _users.size() ; i++ ){
			if ( _users[i]->hasSameLogin(login.c_str()) ){
				cr = NO_ERROR;
				break;
			}
		}
	}
	/* Check password */
	if ( NO_ERROR == cr ){
		if ( true == _users[i]->isCorrectPassword(password) ){
			/* Connect it if ok */
			_connectedUser = _users[i];
		}else{
			cr = ERROR_SEC;
		}
	}
	return cr;
}

myerror_t API::disconnect()
{
	myerror_t cr = NO_ERROR;
	if ( NULL == _connectedUser ){
		cr = ERROR_SEQ;
	}else{
		_connectedUser = NULL;
	}
	return cr;
}

myerror_t API::newUser(const std::string &login, const std::string &password, uint32_t *id)
{
	myerror_t cr = NO_ERROR;
	User *user = NULL;
	
	if ( NULL != id )
		*id = 0;
	/* Test somebody is connected */
	if ( NULL == _connectedUser ){
		cr = ERROR_SEQ;
	}

	/* Test for admin rights */
	if ( NO_ERROR == cr ){
		if ( USER_ADMIN != _connectedUser->getRole() ){
			cr = ERROR_SEC;
		}
	}

	/* If ok create user */
	if ( NO_ERROR == cr ){
		uint32_t new_id = _users.back()->getId() + 1;
		user = new User(login.c_str(),password.c_str(),new_id,USER_BASIC);
		cr = addUser(user);
	}
	if ( NO_ERROR == cr ){
		if ( NULL != id )
			*id = user->getId();
	}
	return cr;
}

myerror_t API::deleteUser(uint32_t id)
{
	myerror_t cr = NO_ERROR;
	
	/* Test somebody is connected */
	if ( NULL == _connectedUser ){
		cr = ERROR_SEQ;
	}
	
	/* Test for admin rights */
	if ( NO_ERROR == cr ){
		if ( USER_ADMIN != _connectedUser->getRole() ){
		cr = ERROR_SEC;
		}
	}
	
	/* If ok delete user */
	if ( NO_ERROR == cr ){
		auto it = _users.begin();
		for (  ; it != _users.end() ; it ++ ){
			if ( (*it)->getId() == id ) // found
				break;
		}
		if ( it == _users.end() ){
			cr = ERROR_PARAM;
		}else{
			_users.erase(it);
			/* Erase corresponding keys */
			for ( auto it = _keys.begin() ; it != _keys.end() ; ){
				if ( (*it)->_owner == id ){
					_keys.erase(it);
				}else{
					it ++;
				}
			}
		}
	}
	return cr;
}

myerror_t API::newKey(key_type_t type, key_use_t use, date_t validity, uint32_t length, uint32_t *id)
{
	myerror_t cr = NO_ERROR;
	if ( NULL != id )
		*id = 0;
	/** <li> Only symmetric keys are implemented at the moment </li> */
	if ( KEY_SYM != type ){
		cr = ERROR_PARAM;
	} else { 
		/** <li> Check key size </li> */
		if ( (0 == length) || ( Key::_maxLength < length) ){
			cr = ERROR_PARAM;
		}
		/** <li> Check use </li> */
		else if ( (KEY_ENCR != use) &&
				(KEY_SIGN != use) &&
				(KEY_INTE != use) &&
				(KEY_WRAP != use) ){
			cr = ERROR_PARAM;
		}
	}
	if ( NO_ERROR == cr ){
		Key *newkey = new Key();
		/** <li> Setting attributes </li> */
		newkey->_type = type;
		newkey->_use = use;
		newkey->_validity = validity;
		newkey->_length = length;
		newkey->_associated = 0; // symmetric keys have no associated key
		newkey->_owner = _connectedUser->getId();
		newkey->_id = _keys.back()->_id + 1;
		/** <li> Generating the value </li> */
		newkey->_value.clear();
		srand(time(NULL)+newkey->_id);
		for ( unsigned i = 0 ; i < length ; i++ ){
			newkey->_value.push_back(rand()&256);
		}
		addKey(newkey);
		if ( NULL != id )
			*id = newkey->_id;
	}
	
	return cr;
}

myerror_t API::wrap(uint32_t idWrapKey, uint32_t idExportedKey, uint8_t *output, uint32_t *length)
{
	/** <ol> */
	myerror_t ret = NO_ERROR;
	Key *wrap = NULL, *exported = NULL;
	uint8_t wrap_key[Key::_maxLength], exported_key[Key::_maxLength];
	uint8_t bloc[16];
	uint32_t allocatedLength = Key::_maxLength;

	memset(wrap_key,0,allocatedLength);
	memset(exported_key,0,allocatedLength);

	/** <li> Get the keys </li> */
	if ( 	(findKey(idWrapKey,&wrap) != NO_ERROR) ||
			(findKey(idExportedKey,&exported) != NO_ERROR) ){
		ret = ERROR_PARAM;
	}
	/** <li> Test wrap key use and output size </li> */
	if ( ret = NO_ERROR ){
		if ( KEY_WRAP != wrap->getUse() ){
			ret = ERROR_PARAM;
		}
		if ( *length < exported->_length ){
			ret = ERROR_PARAM;
		}
	}
	/** <li> Test that the connected user owns the keys </li> */
	if ( ret == NO_ERROR ){
		if ( 	(wrap->getOwner() != _connectedUser->getId()) ||
				(exported->getOwner() != _connectedUser->getId()) ){
			ret = ERROR_SEC;
		}
	}
	/** <li> Wrap key </li> */
	if ( ret == NO_ERROR ){
		decryptKey(wrap,wrap_key,&allocatedLength);
		decryptKey(exported,exported_key,&allocatedLength);
		uint32_t index = 0;
		do{
			AES::encrypt(wrap_key,exported_key+index,bloc);
			for ( unsigned i = 0 ; i < 16 ; i ++ ){
				if ( index >= exported->_length )
					break;
				output[index] = bloc[i];
				index ++;
			}
		}
		while(index < exported->_length)
			;
		*length = exported->_length;
	}
	else{
		*length = 0;
	}

	/** </ol> */
	return ret;
}

myerror_t API::unwrap(uint32_t idWrapKey, uint8_t *importedKey, uint32_t length, key_type_t type, key_use_t use, date_t validity, uint32_t *id)
{
	/** <ol> */
	myerror_t ret = NO_ERROR;
	Key *wrap = NULL;
	uint8_t wrap_key[Key::_maxLength], imported_key[Key::_maxLength];
	uint8_t bloc[16];
	uint32_t allocatedLength = Key::_maxLength;

	memset(wrap_key,0,allocatedLength);
	memset(imported_key,0,allocatedLength);

	/** <li> Get the key </li> */
	if ( NO_ERROR != findKey(idWrapKey,&wrap) ){
		ret = ERROR_PARAM;
	}
	/** <li> Test that the connected user owns the key </li> */
	if ( NO_ERROR == ret ){
		if ( wrap->getOwner() != _connectedUser->getId() ){
			ret = ERROR_SEC;
		}
	}
	/** <li> Test wrap key use and import size </li> */
	if ( NO_ERROR == ret ){
		if ( KEY_WRAP != wrap->getUse() )
			ret = ERROR_PARAM;
		if ( length > Key::_maxLength )
			ret = ERROR_PARAM;
	}
	/** <li> Unwrap key and protect it with local key </li> */
	if ( NO_ERROR == ret ){
		decryptKey(wrap,wrap_key,&allocatedLength);
		uint32_t index = 0;
		do{
			AES::decrypt(wrap_key,importedKey+index,bloc);
			for ( unsigned i = 0 ; i < 16 ; i ++ ){
				if ( index >= length )
					break;
				imported_key[index] = bloc[i];
				index ++;
			}
		}
		while(index < length)
			;
	}
	/** <li> Add key to API </li> */
	Key *newkey = new Key();
	/** <li> Setting attributes </li> */
	newkey->_type = type;
	newkey->_use = use;
	newkey->_validity = validity;
	newkey->_length = length;
	newkey->_associated = 0; // symmetric keys have no associated key
	newkey->_owner = _connectedUser->getId();
	newkey->_id = _keys.back()->_id + 1;
	newkey->_value.clear();
	protectData(imported_key,length);
	for ( unsigned i = 0 ; i < length ; i++ ){
		newkey->_value.push_back(imported_key[i]);
	}
	addKey(newkey);
	if ( NULL != id )
		*id = newkey->_id;

	/** </ol> */
	return ret;
}

myerror_t API::encrypt(uint32_t idKey, uint8_t *buffer, uint32_t length)
{
	/** <ol> */
	myerror_t ret = NO_ERROR;
	Key *encryption = NULL;
	uint8_t encryption_key[Key::_maxLength];
	uint8_t bloc[16];
	uint32_t allocatedLength = Key::_maxLength;
	
	memset(encryption_key,0,allocatedLength);
	
	/** <li> Get the key </li> */
	if ( NO_ERROR != findKey(idKey,&encryption) ){
		ret = ERROR_PARAM;
	}
	/** <li> Test that the connected user owns the key </li> */
	if ( NO_ERROR == ret ){
		if ( encryption->getOwner() != _connectedUser->getId() ){
			ret = ERROR_SEC;
		}
	}
	/** <li> Test key use and type</li> */
	if ( NO_ERROR == ret ){
		if ( (KEY_ENCR != encryption->getUse()) || (KEY_SYM != encryption->getType()) ){
			ret = ERROR_PARAM;
		}
	}
	/** <li> Encrypt </li> */
	if ( NO_ERROR == ret ){
		decryptKey(encryption,encryption_key,&allocatedLength);
		uint32_t index = 0;
		do{
			AES::encrypt(encryption_key,buffer+index,bloc);
			for ( unsigned i = 0 ; i < 16 ; i ++ ){
				if ( index >= length )
					break;
				buffer[index] = bloc[i];
				index ++;
			}
		}
		while(index < length);
	}
	
	/** </ol> */
	return ret;
}

myerror_t API::decrypt(uint32_t idKey, uint8_t *buffer, uint32_t length)
{
	/** <ol> */
	myerror_t ret = NO_ERROR;
	Key *decryption = NULL;
	uint8_t decryption_key[Key::_maxLength];
	uint8_t bloc[16];
	uint32_t allocatedLength = Key::_maxLength;
	
	memset(decryption_key,0,allocatedLength);
	
	/** <li> Get the key </li> */
	if ( NO_ERROR != findKey(idKey,&decryption) ){
		ret = ERROR_PARAM;
	}
	/** <li> Test that the connected user owns the key </li> */
	if ( NO_ERROR == ret ){
		if ( decryption->getOwner() != _connectedUser->getId() ){
			ret = ERROR_SEC;
		}
	}
	/** <li> Test key use and type</li> */
	if ( NO_ERROR == ret ){
		if ( (KEY_ENCR != decryption->getUse()) || (KEY_SYM != decryption->getType()) ){
			ret = ERROR_PARAM;
		}
	}
	/** <li> Encrypt </li> */
	if ( NO_ERROR == ret ){
		decryptKey(decryption,decryption_key,&allocatedLength);
		uint32_t index = 0;
		do{
			AES::decrypt(decryption_key,buffer+index,bloc);
			for ( unsigned i = 0 ; i < 16 ; i ++ ){
				if ( index >= length )
					break;
				buffer[index] = bloc[i];
				index ++;
			}
		}
		while(index < length);
	}
	
	/** </ol> */
	return ret;
}

