/** \file api.h
 * Secure API interface definition.
 *
 * Secure API interface definition.
 */

#ifndef API_H
#define API_H

#include <string>
#include "error.h"
#include "user.h"
#include "key.h"

#define CMD_HEADER_MASK 0xFFFF0000
#define CMD_HEADER      0x80000000

#define API_CMD_MASK    0x0000FFFF
#define API_PING            0x0011
#define API_RESET           0x00FF
#define API_LOAD            0x0101
#define API_SAVE            0x0102
#define API_ADD_USR         0x0201
#define API_DEL_USR         0x0202
#define API_LOGIN           0x0301
#define API_LOGOUT          0x0302
#define API_GEN_KEY         0x0401
#define API_IMP_KEY         0x0402
#define API_EXP_KEY         0x0403
#define API_DAT_ENC         0x0501
#define API_DAT_DEC         0x0502
#define API_DAT_HASH        0X0503

class API
{
	public:
		API() : _connectedUser(NULL) { this->reset(); }
		~API() { }
		myerror_t load(const std::string &userFile, const std::string &keyFile);
		myerror_t save(const std::string &userFile, const std::string &keyFile);

		/** \brief Resets the API
		  *
		  * Resets the API to its original state (one admin, one user and two keys).
		  * <HR>
		  */
		myerror_t reset();

		/** \brief Connects a user to the API
		  * <HR>
		  * \param login is a string containing the login of the user to connect
		  * \param password contains the corresponding password
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if user not in the base
		  * <li> #ERROR_SEQ if a user is already connected
		  * </ul>
		  * <HR>
		  */
		myerror_t connectUser(const std::string &login, const std::string &password);
		
		/** \brief Disconnects the current user from the API
		  * <HR>
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_SEQ if nobody was connected
		  * </ul>
		  * <HR>
		  */
		myerror_t disconnect();

		/** \brief Register a new user to the API
		  *
		  * Add the user to the list of API users.
		  * <HR>
		  * \param login is a string containing the login of the user to create
		  * \param password contains the corresponding password
		  * \param id is a pointer to a variable that will get the id of the created user or be set to 0 in case of error (id could be NULL)
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured </li>
		  * <li> #ERROR_SEC if current user has not admin rights </li>
		  * <li> #ERROR_SEQ if nobody is connected </li>
		  * <li> #ERROR_PARAM if user is already in the base </li>
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t newUser(const std::string &login, const std::string &password, uint32_t *id);

		/** \brief Delete the user from the API
		  *
		  * Deleting a user from the user list (admin rights required).
		  * <HR>
		  * \param id is the id of the user to delete
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured </li>
		  * <li> #ERROR_SEC if connected user is not admin or if he tries to delete himself </li>
		  * <li> #ERROR_PARAM if user is not in the base </li>
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t deleteUser(uint32_t id);

		/** \brief Randomly generate a key and add it to the memory
		  *
		  * <HR>
		  * \param type should be one of the key_type_t values
		  * \param use should be one of the key_use_t values
		  * \param validity could be anything
		  * \param length should not be 0 and should not exceed the maximal size of a key #MAX_KEY_LEN
		  * \param id will be set to 0 in case of error or will contain the newly created key id (could be NULL)
		  * \return
		  * <ul>
		  * <li> #NO_ERROR if no error occured </li>
		  * <li> #ERROR_PARAM if invalid attributes are given </li>
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t newKey(key_type_t type, key_use_t use, date_t validity, uint32_t length, uint32_t *id);

		/** \brief Wrap a key using AES
		  * <HR>
		  * \param idWrapKey is the id of the key that will be used for wrapping
		  * \param idExportedKey id the id of the key to export
		  * \param output is a byte array that will contain the wrapped key
		  * \param length is a pointer to the length of the byte array. Eventually, it will be set to the size of the wrapped key.
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if one key is unknown or if keys are of a wrong type/use or if length is too small
		  * <li> #ERROR_SEC if unauthorized users is trying to use a key
		  * </ul>
		  * <HR>
		  */
		myerror_t wrap(uint32_t idWrapKey, uint32_t idExportedKey, uint8_t *output, uint32_t *length);
		
		/** \brief Unwrap a key using AES
		  * <HR>
		  * \param idWrapKey is the id of the key that will be used for wrapping
		  * \param importedKey is a byte array that will contain the wrapped key
		  * \param length is a pointer to the length of the byte array.
		  * \param type should be one of the key_type_t values
		  * \param use should be one of the key_use_t values
		  * \param validity could be anything
		  * \param id will be set to 0 in case of error or will contain the newly created key id (could be NULL)
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if one key is unknown or if keys are of a wrong type/use
		  * <li> #ERROR_SEC if unauthorized users is trying to use a key
		  * </ul>
		  * <HR>
		  */
		myerror_t unwrap(uint32_t idWrapKey, uint8_t *importedKey, uint32_t length, key_type_t type, key_use_t use, date_t validity, uint32_t *id);

		/** \brief Encrypt message using AES (ECB)
		  * <HR>
		  * \param idKey is the id of the key that will be used for encryption
		  * \param buffer is a byte array that contains the plaintext and will eventually contain the corresponding ciphertext
		  * \param length is the length of the provided byte array.
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if the key is unknown or it is of a wrong type/use or if length is too large 
		  * <li> #ERROR_SEC if unauthorized users is trying to use a key
		  * </ul>
		  * <HR>
		  */
		myerror_t encrypt(uint32_t idKey, uint8_t *buffer, uint32_t length);

		/** \brief Decrypt message using AES (ECB)
		  * <HR>
		  * \param idKey is the id of the key that will be used for encryption
		  * \param buffer is a byte array that contains the ciphertext and will eventually contain the corresponding plaintext
		  * \param length is the length of the provided byte array.
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if the key is unknown or it is of a wrong type/use
		  * <li> #ERROR_SEC if unauthorized users is trying to use a key
		  * </ul>
		  * <HR>
		  */
		myerror_t decrypt(uint32_t idKey, uint8_t *buffer, uint32_t length);

		std::string _defaultUserFile = "default_users.xml";
		std::string _defaultKeyFile = "default_keys.xml";
		std::string _userFile = "current_users.xml";
		std::string _keyFile = "current_keys.xml";
	
	private:
		/** \brief Id of the API instance (used to identify this instance and to ensure user and key files are corresponding to the same instance */
		uint32_t _instanceId;
		
		/** \brief Identity of the currently connected user (0 if no user connected). */
		User * _connectedUser;
		
		/** \brief List of registered users */
		std::vector<User*> _users;
		/** \brief Memory containing all keys that may be manipulated by the API */
		std::vector<Key*> _keys; 

		/** \brief Add a key to the memory
		 *
		 * <HR>
		 * \param key is a pointer to the Key class that should be added to the memory.
		 * \return #NO_ERROR
		 *
		 * <HR>
		 */
		myerror_t addKey(Key *key);

		/** \brief Add a user to the base
		 *
		 * Add the user to the list of API users.
		 * <HR>
		 * \param user is a pointer to the User class that should be registered.
		 * \return 
		 * <ul>
		 * <li> #NO_ERROR if no error occured 
		 * <li> #ERROR_PARAM if user is already in the base
		 * </ul>
		 *
		 * <HR>
		 */
		myerror_t addUser(User *user);

		/** \brief This function finds a key corresponding to a given id.
		  *
		  * <HR>
		  * \param id is the id of the searched key
		  * \param key is a pointer to a key* object that will be set to point to the found key
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if no key is found
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t findKey(uint32_t id, Key **key);

		/** \brief This function encrypts the data using the local encryption key.
		  *
		  * <HR>
		  * \param data is a pointer to the data to protect
		  * \param length is the length of the data (in bytes).
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t protectData(uint8_t *data, uint32_t length);

		/** \brief This function decrypts the key using the local encryption key. IMPORTANT: erase buffer after use !
		  *
		  * <HR>
		  * \param key is a pointer to the key to decrypt
		  * \param output will contain the key value
		  * \param length is a pointer to the length of the byte array that will be set to the final size of the wrapped key. 
		  * \return 
		  * <ul>
		  * <li> #NO_ERROR if no error occured 
		  * <li> #ERROR_PARAM if ouptut is too small
		  * </ul>
		  *
		  * <HR>
		  */
		myerror_t decryptKey(const Key *key, uint8_t *output, uint32_t *length);

};

#endif
