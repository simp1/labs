/** \file error.h
 * Error codes definition
 *
 * Error codes definition
 */

#ifndef ERROR_H
#define ERROR_H

/** \brief Error codes
 *
 * */

enum myerror_t{
  /** \brief evertything is ok */
  NO_ERROR = 0,
  /** \brief an I/O error occured */
  ERROR_IO = 1 << 0,
  /** \brief provided parameters do not match pre-conditions*/
  ERROR_PARAM = 1 << 1,
  /** \brief the data provided did not match the expected format */
  ERROR_FORMAT = 1 << 2,
  /** \brief a security error occured */
  ERROR_SEC = 1 << 3,
  /** \brief a sequencing error occured */
  ERROR_SEQ = 1 << 4,
   /** \brief a memory error occured */
  ERROR_MEM = 1 << 5,
  /** \brief an unexpected error occured */
  ERROR_UNEXPECTED = -1,
};

#endif
