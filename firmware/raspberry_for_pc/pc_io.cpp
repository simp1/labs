#include "io.h"

#include <cstdio>
#include <cstring>

Serial::Serial(const char *port_name){
	// nothing to do
}

Serial::~Serial(){
	// nothing to do
}

myerror_t Serial::send_data(const uint8_t *data, uint32_t len){
	myerror_t rcode = ERROR_UNEXPECTED;
	int i;
	printf("[tx]%02x%02x%02x%02x\n",len&0xFF,(len>>8)&0xFF,(len>>16)&0xFF,(len>>24)&0xFF);
	//printf("[out]%08x\n",len);
	printf("[tx]");
	for ( i = 0 ; i < len ; i ++ )
		printf("%02x",data[i]);
	printf("\n");
	rcode = NO_ERROR;
	return rcode;
}

myerror_t Serial::read_n_bytes(uint8_t *data, uint32_t len){
	int bytes_read = 0;
	// wait for in
	char tag[4] = {0,0,0,0};
	char expected[5] = "[rx]";
	unsigned valid = 0;
	do{
		tag[valid] = getc(stdin);
		if ( tag[valid] == expected[valid] )
			valid ++;
		else{
			valid = 0;
		}
	}
	while( strncmp(tag,"[rx]",4) );
	while ( bytes_read < len ){
		scanf("%02x",data+bytes_read);
		bytes_read ++;
	}
	return NO_ERROR;
}

myerror_t Serial::get_data(uint8_t *data, uint32_t *data_len){
	myerror_t rcode = ERROR_UNEXPECTED;
	// First wait for 4 bytes -> length
	uint32_t size = 0;
	rcode = read_n_bytes((uint8_t*)&size,4);
	if ( size > *data_len ){
		rcode = ERROR_IO;
		printf("too much data (%d) for buffer of size %d\n",size,*data_len);
	}
	if ( NO_ERROR == rcode ){
		rcode = read_n_bytes(data,size);
	}
	if ( NO_ERROR == rcode ){
		//printf("[out]%08x\n",size);
		printf("[tx]%02x%02x%02x%02x\n",size&0xFF,(size>>8)&0xFF,(size>>16)&0xFF,(size>>24)&0xFF);
	}
	*data_len = size;
	return rcode;
}

static uint8_t current_led = 0x0;
static uint8_t current_trig = 0x0;

GPIO::GPIO(){
	// nothing to do
}

GPIO::~GPIO(){
	// nothing to do
}

void GPIO::set_led(uint8_t value){
	if ((current_led ^ value) & GREEN_LED) // changing state
		if (value & GREEN_LED)
			printf("SWITCH GREEN LED ON\n");
		else
			printf("SWITCH GREEN LED OFF\n");
	if ((current_led ^ value) & ORANGE_LED) // changing state
		printf("SWITCH ORANGE LED\n");
	if ((current_led ^ value) & RED_LED) // changing state
		printf("SWITCH RED LED\n");
	current_led = value;
}

void GPIO::set_trigger(bool level){
	if (current_trig != level){
		if (level)
			printf("TRIGGER ON\n");
		else
			printf("TRIGGER OFF\n");
		current_trig = level;
	}
}

