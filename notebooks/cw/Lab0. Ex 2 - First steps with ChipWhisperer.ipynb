{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab0. Ex 2 - First steps with ChipWhisperer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**SUMMARY**: This lab will walk you through your first steps with ChipWhisperer. You will learn how to setup and connect to your target and scope boards, how to build firmware for the target microcontroller, how to configure your scope and capture power traces, and how to communicate with target devices.\n",
    "\n",
    "All the API calls are documented on the [ChipWhisperer readthedocs page](https://chipwhisperer.readthedocs.io/en/latest/api.html). Open it now in another tab and follow along there as well, so you get used to navigate through it to be able to find what you need later on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Physical Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As introduced in the Lab0 handout, you will (probably) work with two ChipWhisperer setups during the course: (1) the single board version of ChipWhisperer-Lite, which contains the scope and target in a unique board; and a the ChipWhisperer-Lite scope board with a CW308 target baseboard. In both cases the target is an Arm Cortex-M4 microntroller."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ChipWhisperer-Lite (Single Board)\n",
    "\n",
    "The ChipWhisperer-Lite is a single-board device. It includes the capture hardware, along with a built in STM32F3  target (other versions of the board contain an XMEGA target, but not the ones we have in the lab). To use this target, simply plug in the device:\n",
    "\n",
    "<img src=\"img/cwlite_plugged.jpg\" alt=\"CW-Lite Plugged In\" width=400>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ChipWhisperer-Lite Capture + UFO baseboard\n",
    "\n",
    "This package uses a ChipWhisperer-Lite Capture board, along with a CW308 UFO target board. You'll need to attach the CW308 board to your ChipWhisperer-Lite Capture, and plug in your choosen target board. The target you will using is the `CW308_STM32F3` (referred to as CW308T-STM32F in other parts of the documentation).\n",
    "\n",
    "Here is an overview photo of the target plugged in:\n",
    "\n",
    "<img src=\"img/cwcapture_ufo.jpg\" alt=\"CW-Lite Plugged Into UFO\" width=600>\n",
    "\n",
    "Attach the ribbon cable to both boards before connecting the target through the USB. This 20-pin connection is a standard connector in NewAE ChipWhisperer family. It contains the power and I/O pins that allow target and scope boards to communicate. More info in: https://rtfm.newae.com/Capture/ChipWhisperer-Lite/\n",
    "\n",
    "You also need to connect the Measure port (JP10) from the scope board to the target Vout port (J17). This connection takes the differential **voltage measurement** from a shunt resistor in the target board to the input of the low-noise amplifier and ADC in the scope board. A shunt resistor is a very low resistance that allows measuring an electric current without producing a large voltage drop. This specific current is the one consumed by our target through its voltage supply pins, which hence relates to the power being consumed. Therefore, the voltage measured through this shunt (and fed to the scope), which is a function of the current drawn by the target from the power supply, gives a measure of the power consumption, and hence allows to run power attacks. \n",
    "\n",
    "The connections carried by these two cables (ribbon and SMA) are also present in the ChipWhisperer-Lite single board as you can imagine. We just don't need to connect them as the target and scope are attached together. In fact, the CWLite single board could be split apart in two, so you could use the scope standalone as in this separate version (after soldering the SMA and 20-pin connectors).\n",
    "\n",
    "See the UFO Target Settings below and make sure all the jumpers and switches are configured as in the figure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### UFO Target Settings\n",
    "\n",
    "The UFO Board comes by default with working settings for most targets. A summary of the default jumper settings is included below for you, see the product documentation for more details.\n",
    "\n",
    "<img src=\"img/cwufo_stm32f3.jpg\" alt=\"UFO Board\" width=500>\n",
    "\n",
    "##### UFO Default Settings\n",
    "\n",
    "* J1  - 3.3V (CW3.3V)\n",
    "* J3  - HS2/OUT\n",
    "* J14 - FILT/FILT_LP/FILT_HP\n",
    "* J16 - No connection\n",
    "\n",
    "* S1 -  ON\n",
    "\n",
    "* 3.3V SRC - J1/CW\n",
    "* VADJ SRC - 3.3V\n",
    "* 1.2V, 1.8V, 2.5V, 3.3V, LDO SRC - J1/CW\n",
    "* 5V SRC - J1/CW"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connecting to ChipWhisperer\n",
    "\n",
    "Now that your hardware is all setup, we can now learn how to connect to it. \n",
    "\n",
    "You use the ChipWhisperer Python library as you would any other Python library. The following cell shows how to import the ChipWhisperer library and show the help information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import chipwhisperer\n",
    "help(chipwhisperer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ChipWhisperer hardware quick test\n",
    "\n",
    "Let's first run a simple test to check everything is OK.\n",
    "\n",
    "The following will connect to a ChipWhisperer. Just plug the Chipwhisperer main board (i.e., the scope). This does not test any possibly attached target boards, just tests the communication with the scope. If the ChipWhisperer communication is up, everything else should \"just work\" when talking to various targets.\n",
    "\n",
    "This should result should be something like ``INFO: Found ChipWhisperer😍``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PLATFORM=\"NOTHING\"\n",
    "%run Setup_Scripts/Setup_Generic.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you got the ``INFO: Found ChipWhisperer😍``, you are good to go. You should also see a green LED blinking, which indicates the system is up and running (it is the FPGA Heartbeat LED). Otherwise please let us know. \n",
    "\n",
    "Before continuing, please run the next cell, it you will learn why a bit below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scope.dis()\n",
    "target.dis()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connecting to the scope\n",
    "\n",
    "**Connecting to the scope board** is as simple as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import chipwhisperer as cw\n",
    "scope = cw.scope()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, ChipWhisperer will try to autodetect the type of scope board you have connected. You can also manually specify the scope type (CWLite/CW1200 or CWNano). If you have multiple ChipWhisperer devices connected, you have to specify the serial number of the device you want to connect to:\n",
    "\n",
    "```python\n",
    "scope = cw.scope(sn='<some long string of numbers>')\n",
    "```\n",
    "\n",
    "For more information, see the API section on readthedocs.\n",
    "\n",
    "As with any oscilloscope, there are some things you can configure for your capturing process. The cell below applies a default configuration setup for the scope:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scope.default_setup()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see what it does in the [documentation](https://chipwhisperer.readthedocs.io/en/latest/scope-api.html#chipwhisperer.scopes.OpenADC.default_setup), but basically:\n",
    "\n",
    "* Sets the scope gain to 45dB\n",
    "* Sets the scope to capture 5000 samples\n",
    "* Sets the scope offset to 0 (aka it will begin capturing as soon as it is triggered)\n",
    "* Sets the scope trigger to rising edge\n",
    "* Outputs a 7.37MHz clock to the target on HS2\n",
    "* Clocks the scope ADC at 4\\*7.37MHz. Note that this is *synchronous* to the target clock on HS2\n",
    "* Assigns GPIO1 as serial RX\n",
    "* Assigns GPIO2 as serial TX:\n",
    "\n",
    "### Connecting to targets\n",
    "\n",
    "**Connecting to the target device** is simple as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target = cw.target(scope, cw.targets.SimpleSerial) #cw.targets.SimpleSerial can be omitted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*SimpleSerial* is a serial protocol used to communicate (from *here*) with the target board. We will see more about it later .\n",
    "\n",
    "And that's it! Your ChipWhisperer is now setup and ready to attack a target. \n",
    "\n",
    "**NOTE: You always need to disconnect the scope/target before connecting again from another notebook. It is also a sane way to end your session with ChipWhisperer, so do it everytime you are done with it. This can be done with `scope.dis()` and `target.dis()`**. \n",
    "\n",
    "Now, skip the disconnection process since you will continue working with it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building and Uploading Firmware\n",
    "\n",
    "The next step in attacking a target is to get some algorithm (a.k.a. firmware) built and uploaded onto it. Most firmware for most ChipWhisperer targets can be built using the provided build system in the VM. If you have opted for the native installation, you need to make sure you have the correct compiler installed (see https://chipwhisperer.readthedocs.io/en/latest/prerequisites.html#prerequisites for info about it). So if you are using the VM, you can just continue.\n",
    "\n",
    "Firmware must be built on the command line. Luckily, thanks to Jupyter and the previous exercise, you already know how to run a command from Jupyter as if you were in a terminal.\n",
    "\n",
    "Before that, you want to specify your hardware setup as described through the lab handout. You need to select a `SCOPETYPE`, a `PLATFORM`, and a `CRYPTO_TARGET`. `SCOPETYPE` is `'OPENADC'` for the CWLite we are using. `PLATFORM` is the target device for which you are building, so `'CWLITEARM'` or `'CW308_STM32F3'` for this lab, which are in fact equivalent (if you do not provide a `PLATFORM`, the build system will print a list of supported platforms instead). Lastly, for `CRYPTO_TARGET`just select `'NONE'`. Therefore:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PLATFORM = 'CW308_STM32F3' #or 'CWLITEARM'\n",
    "SCOPETYPE = 'OPENADC'\n",
    "CRYPTO_TARGET = 'NONE'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To build your firmware:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash -s \"$PLATFORM\" \"$CRYPTO_TARGET\"\n",
    "cd ../hardware/victims/firmware/simp_1/lab0/\n",
    "make PLATFORM=$1 CRYPTO_TARGET=$2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Uploading the firmware**\n",
    "\n",
    "ChipWhisperer has built in support for STM32F* bootloaders, which can be used as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "fw_dir = '/home/vagrant/work/projects/chipwhisperer/hardware/victims/firmware/simp_1/lab0/'\n",
    "fw_filename = 'lab0-{}.hex'.format(PLATFORM)\n",
    "fw_path = os.path.join(fw_dir,fw_filename)\n",
    "\n",
    "# Complete the following line to program your target\n",
    "# You will need to use the program_target() method, described in the readthedocs and (lightly) in the lab handout\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Communication with the Target\n",
    "\n",
    "For the targets we are using, the communication is done through the `SimpleSerial target` object we got earlier. SimpleSerial is a very basic serial protocol which can be easily implemented on most systems. This system communicates using a standard asyncronous serial protocol (38400bps, ASCII encoded). You can find its descrption in https://chipwhisperer.readthedocs.io/en/latest/simpleserial.html.\n",
    "\n",
    "The communication is always initiated by the capture board, which sends commands encapsulated in serial packets to the target board. The firmware that runs in the target board has to include the simpleserial protocol, as is the case with the one we build and uploaded. You can see this in the [lab0.c](hardware/victims/firmware/simp_1/lab0/lab0.c) source file, with the `#include \"simpleserial.h\"` and different simpleserial function calls, like `simpleserial_init();`.\n",
    "\n",
    "After `main()`starts the following lines *register* three commands and associate them with a so-called *callback* function.\n",
    "\n",
    "```C\n",
    "#if SS_VER != SS_VER_2_1\n",
    "\tsimpleserial_addcmd('p', 16, get_pt);\n",
    "\tsimpleserial_addcmd('k', 16, get_key);\n",
    "\tsimpleserial_addcmd('x', 0, reset);\n",
    "```\n",
    "\n",
    "The callback functions associated with each of the commands `'p'`, `'k'` and `'x'`, are, `get_pt`, `get_key` and `reset`, respectively. What this means is that everytime the capture board (us) sends one of these commands, the associated function is executed. Usually, `'p'` is associated to sending the plaintext to our cipher, while `'k'` would send the key; it is, however, totally up to you what to do in each case. There are only 3 reserved commands that you cannot use (see the documentation in the readthedocs link).\n",
    "\n",
    "After configuring the simpleserial commands in your firmware, this remains in an infinite loop waiting for commands:\n",
    "\n",
    "```C\n",
    "while(1)\n",
    "    simpleserial_get();\n",
    "```\n",
    "\n",
    "Now, there are two ways to communicate with your simpleserial target:\n",
    "\n",
    "1. Raw serial via `target.read()`, `target.write()`, `target.flush()`, etc. \n",
    "\n",
    "1. SimpleSerial commands via `target.simpleserial_read()`, `target.simpleserial_write()`, `target.simpleserial_wait_ack()`, etc.\n",
    "\n",
    "All messages are sent in ASCII-text, and are normally terminated with a line-feed (`'\\n'`). This allows you to interact with the simpleserial system over a standard terminal emulator.\n",
    "\n",
    "We start demonstrating how to interact using the simpleserial class. Later, we will do the same with the raw serial commands to send the same messages at a lower level.\n",
    "\n",
    "If you check the lab0e firmware (`lab0.c`) you will see that for the simpleserial `'p'` command, the target will just echo back the command we sent. \n",
    "\n",
    "```C\n",
    "simpleserial_put('r', 16, pt);\n",
    "```\n",
    "\n",
    "Note that the ChipWhisperer Lite has a 128 byte read buffer and a 128 byte send buffer, so make sure to format your commands properly.\n",
    "\n",
    "Let's try that out now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg = bytearray([5]*16) #simpleserial uses bytearrays to encode messages\n",
    "target.simpleserial_write('p', msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check if we got a response:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(target.simpleserial_read('r', 16))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try sending now the `'k'` command with a different message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here ...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That command doesn't return anything to us, but it should ack and give us an error return:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(target.simpleserial_wait_ack()) #should return 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see in the documentation, simpleserial messages take the form:\n",
    "\n",
    "```python\n",
    "command_character + ascii_encoded_bytes + '\\n'\n",
    "```\n",
    "\n",
    "For our first command, let `command_character='p'` and `ascii_encoded_bytes=\"00\"*32`. Keep in mind this is not a binary `0x00`, but an ASCII `\"00\"`, which has a binary value of `0x3030`. Try resending the same `'p'` command from before but using now `target.write()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Format your message and send\n",
    "# Your code here ...\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, a simple `target.read()` will return all the characters that have been sent back from the target so far. Let's see what the device returned to us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "recv_msg = \"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "recv_msg += target.read() #you might have to run this block a few times to get the full message\n",
    "print(recv_msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, did you get the expected result? No? Why?\n",
    "Does the message you are sending have the expected format?\n",
    "The expected size?\n",
    "Add another cell below this one and try again with new code.\n",
    "Continue once it is working. Otherwise, let us know.\n",
    "\n",
    "The simpleserial commands are usually sufficient for taking to simpleserial firmware, but you'll need the raw serial commands for some of the other labs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Capturing Traces\n",
    "\n",
    "Now that the target's programmed and we know how to communicate with it, let's start recording some power traces! To capture a trace:\n",
    "\n",
    "1. Arm the ChipWhisperer with `scope.arm()`. It will begin capturing as soon as it is triggered (which in our case is a rising edge on `gpio4`.\n",
    "1. `scope.capture()` will read back the captured power trace, blocking until either ChipWhisperer is done recording, or the scope times out. Note that the error return will tell you whether or not the scope timed out. It does not return the captured scope data.\n",
    "1. You can read back the captured power trace with `scope.get_last_trace()`.\n",
    "\n",
    "`lab0` firmware will trigger the ChipWhisperer when we send the `'p'` command as you can see in the `get_pt()` associated callback. \n",
    "\n",
    "```C\n",
    "uint8_t get_pt(uint8_t* pt, uint8_t len)\n",
    "{\n",
    "\t/**********************************\n",
    "\t* Start user-specific code here. */\n",
    "\ttrigger_high();\n",
    "\n",
    "\t//16 hex bytes held in 'pt' were sent\n",
    "\t//from the computer. Store your response\n",
    "\t//back into 'pt', which will send 16 bytes\n",
    "\t//back to computer. Can ignore of course if\n",
    "\t//not needed\n",
    "\n",
    "\ttrigger_low();\n",
    "\t/* End user-specific code here. *\n",
    "\t********************************/\n",
    "\tsimpleserial_put('r', 16, pt);\n",
    "\treturn 0x00;\n",
    "}\n",
    "```\n",
    "\n",
    "ChipWhisperer will capture the power activity (as many samples as configured before) when the trigger is activated. In fact, it will continue capturing after the `trigger_low()` until getting all the samples. As you can derive from the firmware code, you need to add your algorithm within the trigger activation/deactivation.\n",
    "\n",
    "What you usually would want to do is sending at least a plaintext to your cipher firmware (that can also be a pair of fixed key and random text), which would start the ciphering process. Remember that in the case of this initial firmware sending a key has no effect.\n",
    "\n",
    "Try capturing a trace now after sending the plaintext and key pair (usually, you would also want to get the response from the target sending a `'r'` command):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here ...\n",
    "key = ...\n",
    "text = ...\n",
    "...\n",
    "trace = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now plot the captured trace. Think that your firmware is basically doing nothing, but you are still capturing some activity due to noise, besides any code executed after the `trigger_low()` and untill all the samples are catpured (which possible includes the activity due to the `simpleserial_put('r', 16, pt);` and beyond."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here ...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try configuring the scope with different numbers of samples and plot the results afterwards. \n",
    "\n",
    "Remember that you can select a range of samples from a captured trace before plotting or doing some further analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Alternative capture method**\n",
    "\n",
    "ChipWhisperer also has a `capture_trace()` convience function (https://chipwhisperer.readthedocs.io/en/latest/capture-api.html#) that:\n",
    "\n",
    "1. Optionally sends the `'k'` command\n",
    "1. Arms the scope\n",
    "1. Sends the `'p'` command\n",
    "1. Captures the trace\n",
    "1. Reads the return `'r'` message\n",
    "1. Returns a `Trace` class that groups the trace data (Trace.wave), the `'p'` message (Trace.textin), the `'r'` message (Trace.textout), and the `'k'` message (Text.key)\n",
    "\n",
    "It isn't always the best option to use, but it's usually sufficient for most simpleserial applications."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here ...\n",
    "key = ...\n",
    "text = ...\n",
    "trace = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now get the 4 fields from the trace as returned by `capture_trace()` and plot the trace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here ...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## End of the lab\n",
    "\n",
    "As a final step, we should disconnect from the hardware so it doesn't stay \"in use\" by this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scope.dis()\n",
    "target.dis()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
