/** \file key.h
 * Key definition
 *
 * Key class definition.
 */

#ifndef KEY_H
#define KEY_H

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "error.h"

/** \brief Key types */
enum key_type_t{
	/** \brief symmetric key */
	KEY_SYM = 1,
	/** \brief public key */
	KEY_PUB = 2,
	/** \brief private key */
	KEY_PRI = 3
};

/** \brief Key use	*/
enum key_use_t{
	/** \brief encryption/decryption key (for data) */
	KEY_ENCR = 1,
	/** \brief signature key (for data) */
	KEY_SIGN = 2,
	/** \brief integrity key (for data) */
	KEY_INTE = 3,
	/** \brief wrapping key (for keys) */
	KEY_WRAP = 4
};

typedef uint32_t date_t;

class API;

/** \brief Key class */
class Key
{
public:
	friend class API;
	/** \brief Default constructor: used to create an empty Key before updating it from a JSON string for instance. 
	 *
	 * <HR>
	 */
	Key() : _id(0) , _owner(0) , _type(key_type_t(0)) , _use(key_use_t(0)) , _validity(0) , _associated(0) , _length(0) {};
	/** \brief Key destructor 
	 *
	 * <HR>
	 */
	~Key() {}
	/** \brief This function set the given attribute to the provided value in class Key.
	 *
	 * This function is for example used to construct Key classes from a json_object_t.
	 * <HR>
	 * \param field is a string containing the name of the attribute
	 * \param value is the value that should be set to the mentioned attribute.
	 * \return 
	 * <ul>
	 * <li> #NO_ERROR if no error occured 
	 * </ul>
	 *
	 * <HR>
	 */
	myerror_t set(const std::string &field, const std::string &value);

	/** \brief Function returning the id of the key 
	 *
	 * <HR>
	 */
	uint32_t getId() const;
	
	/** \brief Function returning the owner of the key 
	 *
	 * <HR>
	 */
	uint32_t getOwner() const;
	
	/** \brief Function returning the type of the key 
	 *
	 * <HR>
	 */
	key_type_t getType() const;
	
	/** \brief Function returning the usage of the key 
	 *
	 * <HR>
	 */
	key_use_t getUse() const;
	
	/** \brief Function returning the validity period of the key 
	 *
	 * <HR>
	 */
	date_t getValidity() const;
	
	/** \brief Function returning the (encrypted) value of the key 
	 *
	 * <HR>
	 */
	std::vector<uint8_t> getValue() const;
	
	/** \brief Serialize the Key object to a byte array of \a _serializedUserSize bytes */
	myerror_t serialize(uint8_t *tab, uint32_t len);
	
	/** \brief This function parses an object to update an empty User class previously created.
	 *
	 * This function is for example used to construct User classes when loading the API from a file.
	 * <HR>
	 * \param in is the pointer to the input object containing the user data.
	 * \return an error code.
	 *
	 * <HR>
	 */
	template<class T>
	myerror_t importFromObject(const T* in);

	/** \brief This function export a User in an object.
	 *
	 * This function is for example used to export User classes when saving the API to a file.
	 * <HR>
	 * \param out is the pointer to the output object that will contain the user data. It must have been allocated before the call.
	 * \return an error code.
	 *
	 * <HR>
	 */
	template<class T>
	myerror_t exportToObject(T* out) const;

	/** \brief Maximum length of a key. */
	static constexpr unsigned _maxLength = 64;

	/** \brief Size of the serialization of a Key object without its value */
	static const uint32_t _serializedSize = 4 + 4 + 1 + 1 + 4 + 4 + sizeof(uint32_t);
	
	/** \brief If the corresponding private/public key is in the database: contains its id. */
	uint32_t _associated;
	/** \brief Number of bytes of the key. */
	uint32_t _length;
private :
	/** \brief Id of the key, must be unique. */
	uint32_t _id;
	/** \brief Id of the key owner. */
	uint32_t _owner;
	/** \brief Type of the key. */
	key_type_t _type;
	/** \brief Use of the key. */
	key_use_t _use;
	/** \brief Date when the key will not be valid anymore. */
	date_t _validity;
	/** \brief Value of the key. */
	std::vector<uint8_t> _value;
};

#endif
