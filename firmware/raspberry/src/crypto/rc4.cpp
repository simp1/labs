#include "rc4.h"

#include <cstdio>

myerror_t RC4::test()
{
	myerror_t err = ERROR_UNEXPECTED;
	unsigned i = 0;
	uint8_t data[16];

	/** Testing encryption */
	for ( i = 0 ; i < 16 ; i ++ )
		data[i] = 0;
	encrypt(5,TV_key,16,data);
	for ( i = 0 ; (i < 16) && (data[i] == TV_stream[i]) ; i ++ )
		;
	if ( i != 16 )
		goto end;
	if ( i == 16 )
		err = NO_ERROR;
	
 end:
	return err;
}

myerror_t RC4::encrypt(uint32_t key_size, const uint8_t *key, uint32_t plain_size, 
			uint8_t *data)
{
	myerror_t err = ERROR_UNEXPECTED;
	// key schedule
	uint8_t S[256];
	uint8_t temp;
	unsigned i, j;
	for ( i = 0 ; i < 256 ; i++ )
		S[i] = i;
	j = 0;
	for ( i = 0 ; i < 256 ; i++ ) {
		j = (j + S[i] + key[i % key_size]) & 0xFF;
		temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	}
	//ciphering
	i = 0, j = 0;
	uint8_t byte;
	for ( unsigned index = 0 ; index < plain_size ; index ++ ) {
		i = (i + 1) & 0xFF;
		j = (j + S[i]) & 0xFF;
		temp = S[i];
		S[i] = S[j];
		S[j] = temp;
		data[index] ^= S[(S[i] + S[j]) & 0xFF];
	}
	err = NO_ERROR;
	return err;
}

/* Test Vectors */

const uint8_t RC4::TV_key[5] = {
	0x01,0x02,0x03,0x04,0x05
};

const uint8_t RC4::TV_stream[16] = {
	0xb2, 0x39, 0x63, 0x05, 0xf0, 0x3d, 0xc0, 0x27, 
	0xcc, 0xc3, 0x52, 0x4a, 0x0a, 0x11, 0x18, 0xa8

};



