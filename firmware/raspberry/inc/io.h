/** \file serial.h
 * Serial connection.
 *
 * Serial class definition.
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <cstdint>
#include <cstdlib>

#include "error.h"

class Serial
{
	public:
		Serial(const char *port_name);
		~Serial();
		myerror_t send_data(const uint8_t *data, uint32_t len);
		const uint8_t synchro_pattern = 0x55;
		/* data_len contains the size of buffer data */
		myerror_t get_data(uint8_t *data, uint32_t *data_len);
	private:
		myerror_t read_n_bytes(uint8_t *data, uint32_t len);
		int port_descriptor;
};

class GPIO
{
	public:
		GPIO();
		~GPIO();
		void set_led(uint8_t value);
		void set_trigger(bool level);
		
		static const uint8_t LED_OFF = 0x0;
		static const uint8_t GREEN_LED = 0x1;
		static const uint8_t ORANGE_LED = 0x2;
		static const uint8_t RED_LED = 0x4;
};


#endif
