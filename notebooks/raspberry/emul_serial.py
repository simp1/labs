import pexpect
import binascii
import os

EIGHTBITS = 8
PARITY_NONE = None
STOPBITS_ONE = 1

class Serial(object):
    def __init__(self,port,baudrate=None,bytesize=None,parity=None,stopbits=None,timeout=None,debug=False):
        # launch
        path = os.path.dirname(os.path.abspath(__file__))
        prog = os.path.join(path,'secure-api-pc')
        self.target = pexpect.spawn(prog,cwd=os.path.dirname(prog),maxread=10000,timeout=timeout)
        self.target.setecho(False)
        self.debug = debug
    
    def close(self):
        pass
    
    def write(self,data):
        hexa = ''
        for datum in data:
            if type(datum) != int:
                datum = ord(datum)
            hexa += '{:02x}'.format(datum)
        if self.debug: print("[send]",hexa)
        self.target.write('[rx]'+hexa+'\n')
    
    def read(self,size=1):
        self.target.expect('\[tx\]')
        before = self.target.before.decode().rstrip()
        if self.debug and before != "": print("[device] Message:",before)
        hexa = self.target.readline().rstrip()
        if len(hexa) != 2*size: raise Exception("Data length does not match expected length")
        #res = b''
        #for i in range(size):
        #    res = res + chr(int(hexa[i*2:(i+1)*2],16))
        res = []
        for i in range(size):
            res.append(int(hexa[i*2:(i+1)*2],16))
        res = bytearray(res)
        if self.debug: print("[read]",binascii.hexlify(res))
        return res

